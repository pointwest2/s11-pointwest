﻿using System.ComponentModel.DataAnnotations;

namespace s11_selfstudy.Models
{
    public enum Stack
    {
        MEAN,
        MERN,
        Django,
        [Display(Name = "Ruby on Rails")] Rails,
        LAMPP
    }
}
