﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using s11_selfstudy.Models;

namespace s11_selfstudy.Data
{
    public class s11_selfstudyContext : DbContext
    {
        public s11_selfstudyContext (DbContextOptions<s11_selfstudyContext> options)
            : base(options)
        {
        }

        public DbSet<s11_selfstudy.Models.Course> Course { get; set; } = default!;
    }
}
